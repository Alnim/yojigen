//6.28.17
//Socket.io def
let socket = io();

let chatData = document.getElementById('chatData'),
    inputBox = document.getElementById('inputBox'),
    sendBtn = document.getElementById('send');

sendBtn.onclick = function () {
    chatData.innerHTML += "You: " + inputBox.value + "<br>";
    socket.emit('send message', socket.id, inputBox.value);
};

socket.on('recieve', function (id, msg) {
    if (socket.id != id)
        chatData.innerHTML += id + ": " + msg + "<br>";
});




