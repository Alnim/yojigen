/**
 * Created by Alnim on 12/23/15.
 */
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
mongoose.connect("mongodb://Alnim:mmmmmmmm@ds139322.mlab.com:39322/yojigen", {useMongoClient: true});
//mongoose.connect("localhost", "Kyoiku");
var uIP = null;
app.set('port', (process.env.PORT || 80));
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
app.use(express.static(__dirname + '/'));

// Mongoose Schema (kUsers)
var kUserSchema = mongoose.Schema({
    username: {
        type: String,
        unique: true
    },
    password: String,
    loggedIn: Boolean,
    userObject: Object
});
//Mongoose Modal (kUsers)
var kUser = mongoose.model('kusers', kUserSchema);

var user = null;
io.on('connection', function (socket) {
    console.log("A view has been collected " + socket.id);
    kUser.count({}, function (err, count) {
        if (err) {
            console.log(err);
        } else {
            console.log("Here are the number of registered users: " + count);
        }
    });
    kUser.count({
        loggedIn: true
    }, function (err, count) {
        if (err) {
            console.log(err);
        } else {
            console.log("Here are number of logged in users: " + count);
        }
    });

    //*ADMIN USAGE* (./admin.js/.html)
    socket.on("get kyoikudb docs", function () {
        kUser.find({}, function (err, kUsersArray) {
            socket.emit("receive kyoikudb docs", kUsersArray);
        });
    });

    //REGISTER (1): checking username availibility
    socket.on("cck username ava", function (username) {
        kUser.findOne({
            username: username
        }, function (err, foundUser) {
            if (err) {
                console.log(err);
            } else {
                var found = true;
                if (!foundUser) {
                    found = false;
                }
                socket.emit("cck username ava result", found);
            }
        });
    });
    var newUser = new kUser();
    //REGISTER (2): add to db if not a duplicate
    socket.on("register", function (username, password) {
        newUser.username = username;
        newUser.password = password;
        kUser.findOne({
            username: username
        }, function (err, foundUser) {
            if (err) {
                console.log(err);
            } else {
                var found = true;
                if (!foundUser) {
                    found = false;
                    newUser.save(function (err) {
                        if (err) {
                            console.log(err);
                        }
                    });
                    user = newUser;
                    socket.emit("reg. result", found, user._id);
                } else {
                    socket.emit("reg. result", found, null);
                }

            }
        });
    });
    //LOG IN (1) : authenticate log in credentials
    socket.on("auth", function (username, password) {
        kUser.findOne({
            username: username,
            password: password
        }, function (err, foundUser) {
            if (err) {
                console.log(err);
            } else {
                var found = true;
                if (!foundUser) {
                    found = false;
                    socket.emit("auth result", found, null);
                } else {
                    user = foundUser;
                    if (user.loggedIn === true) {
                        socket.emit("auth result", found, "duplex");
                        user = null;
                    } else {
                        socket.emit("auth result", found, user._id);
                    }
                }
            }
        });
    });
    //REGISTER (3) / LOG IN (2): log in to Kyoiku System
    socket.on("logged in successfully", function () {
        user.loggedIn = true;
        user.save(function (err) {
            if (err) {
                console.log(err);
            }
        });
        socket.emit("update client uP object", user.userObject);
        //console.log(socket.id);
    });
    socket.on('send message', function(id, msg){
        io.sockets.emit("recieve", id, msg);
    });
    socket.on("update uP", function (uP) {
        if (user._id == uP.dbId && user.userObject != uP) {

            user.userObject = uP;
            user.ip = uIP;
            //console.log(uIP);
            user.save(function (err) {
                if (err) {
                    console.log(err);
                }
                socket.emit("updated uP", uP);
            });

        }
    });
    //LOG OUT : exit out (for now) (will add a log out btn to go to log in screen again)
    socket.on('disconnect', function () {
        if (user !== null && user.userObject !== null) {
            user.loggedIn = false;
            //get the current QD
            var cQD = user.userObject.db.Q[user.userObject.cqnm];
            if (cQD != null) {
                //get the current mD:
                var cMD = cQD.mD[cQD.mD.length - 1];
                if (cMD != null && cMD.tD != undefined && cMD.tD.eT == null)
                    cQD.mD.pop();
            }
            user.save(function (err) {
                if (err) {
                    console.log(err);
                }
            });
        }

        socket.broadcast.emit('destroy player');
        console.log('a viewer left (but not a view (:)?))');
    });
});

process.stdin.resume();
process.stdin.setEncoding('utf8');

process.stdin.on('data', function (text) {
    console.log(text);
    switch (text.trim()) {
        case "quit":
            done();
    }
    if (text.trim() === 'quit') {
        done();
    }
});

function done() {
    console.log('Now that process.stdin is paused, there is nothing more to do.');
    process.exit();
}

http.listen(app.get('port'), '0.0.0.0', function () {
    console.log(' +' + app.get('port'));
});
